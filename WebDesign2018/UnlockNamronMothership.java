import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import static java.lang.Math.pow;

public class UnlockNamronMothership {
    public static void main(String args[]) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        PasanganAB[] pasanganABs = new PasanganAB[n];

        for (int i = 0; i < n; i++) {
            String[] lines = reader.readLine().split(" ");
            int a = Integer.parseInt(lines[0]);
            int b = Integer.parseInt(lines[1]);
            pasanganABs[i] = new PasanganAB(a, b);
        }

        selectionSort(pasanganABs);
        printHasilSorting(pasanganABs);
    }

    private static void printHasilSorting(PasanganAB[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i].a + " " + arr[i].b);
        }
    }
    private static void selectionSort(PasanganAB[] arr) {
        int n = arr.length;
        int indeksMinimum;
        for(int i=0; i < n; i++) {
            indeksMinimum = i;
            for(int j=i+1; j < n; j++){
                if (arr[j] < arr[indeksMinimum])
                    indeksMinimum = j;
            // TODO: Implementasikan pencarian minimum Anda di sini
            }
            int t = arr[indeksMinimum];
            arr[indeksMinimum] = arr[i];
            a[i] = t;
            // TODO: Implementasikan prosedur setelah pencarian minimum di sini
        }
    }
}

class PasanganAB implements Comparable<PasanganAB> {
    // TODO: Lengkapi class dengan instance variable, constructor, dan method yang sesuai dan menurut Anda diperlukan
    public int a, b;

    public PasanganAB(int a, int b) {
        this.a = a;
        this.b = b;
    }

    // FIXME: Anda mungkin ingin memperbaiki cara kerja method ini
    public int hitungF(int x, int y) {
        for (int i = 0; i < y, i++) {
            if (y == 0) {
                return x;
            }
            else {
                return(4 * Math.pow(hitungF(x, y-1), 2) + 3);
            }
        }
    }

    // FIXME: Anda mungkin ingin memperbaiki cara kerja method ini
    public int hitungK(int x, int y) {
        return(hitungF(x, y) % 17);
    }
    @Override
    public int compareTo(PasanganAB other) {
        // TODO: Lengkapi method ini untuk sorting sesuai dengan spesifikasi soal
    }
}
