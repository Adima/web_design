from django.shortcuts import render
from datetime import datetime
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .forms import post
from .models import Status
import json
import pytz

        
def index(request):
    form = post()
    obj = Status.objects.all()
    if request.method == 'POST':
        form = post(request.POST or None)
    if form.is_valid():
        form.save()
    context = { 'form' :form,
    'obj' :obj}
    return render(request, 'MyStory/index.html', context);