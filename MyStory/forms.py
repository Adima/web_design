from django import forms
from .models import Status

class post(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']