from django.apps import AppConfig


class MystoryConfig(AppConfig):
    name = 'MyStory'
