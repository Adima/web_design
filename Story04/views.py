from django.shortcuts import render
from .models import Form
from .forms import FormForm
import pytz


def index(request):
    return render(request, 'Story04/index.html', {})

def gallery(request):
    return render(request, 'Story04/gallery.html', {})

def bukutamu(request):
    return render(request, 'Story04/bukutamu.html', {})

def form_create_view(request):
    form = FormForm(request.POST or None)
    if form.is_valid():
        form.save()

    context = {
        "form": form
        "object" : object
    }
    return render(request, "Story04/layout.html", context)