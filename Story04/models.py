from django.db import models
from django.utils import timezone

class Form(models.Model):
    Days_and_value = (
        (0, "Monday"),
        (1, "Tuesday"),
        (2, "Wednesday"),
        (3, "Thursday"),
        (4, "Friday"),
        (5, "Saturday"),
        (6, "Sunday"),
    )

    day = models.Charfield(max_length = 1, choices = Days_and_value, blank = True)
    date = models.DateField(blank = True)
    time = models.TimeField(blank = True)
    event = models.CharField(max_length = 120, blank = True)
    place = models.CharField(max_length = 80, blank = True)
    category = models.Charfield(max_length = 50, blank = True)
    def __str__(self):
        return self.title