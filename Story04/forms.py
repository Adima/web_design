from django import forms
from .models import forms

class FormForm(forms.ModelForm):
    class Meta:
        model = Form
        fields = [
            "day",
            "date",
            "time",
            "event",
            "place",
            "category"
        ]