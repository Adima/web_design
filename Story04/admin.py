from django.contrib import admin

from modelforms.models import Product

admin.site.register(Product)
